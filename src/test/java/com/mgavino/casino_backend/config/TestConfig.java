package com.mgavino.casino_backend.config;

import com.mgavino.casino_backend.casino.CasinoApplication;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class TestConfig {

    @Bean
    @Primary
    public CasinoApplication casinoApplication() throws Exception {

        //override CasinoApplication.run() to do nothing when TEST
        CasinoApplication casinoApplication = Mockito.mock(CasinoApplication.class);
        Mockito.doNothing().when(casinoApplication).run();
        return casinoApplication;

    }
}
