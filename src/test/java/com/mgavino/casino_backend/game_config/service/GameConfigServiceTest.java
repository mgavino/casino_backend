package com.mgavino.casino_backend.game_config.service;

import com.mgavino.casino_backend.game_config.repository.GameConfigRepository;
import com.mgavino.casino_backend.game_config.repository.model.GameConfigEntity;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigFilterDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigResponseDto;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameConfigServiceTest {

    @Autowired
    private GameConfigService service;

    @Autowired
    private GameConfigRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(service);
    }

    @Test
    public void insert() throws Exception {

        GameConfigDto gameConfig = TestUtils.createGameConfigDto(UUID.randomUUID(), 10f, 11f, 1000f);

        // call
        GameConfigResponseDto gameConfigResponseDto = service.insert(gameConfig);

        // check
        Assert.assertNotNull(gameConfigResponseDto);
        Assert.assertNotNull(gameConfigResponseDto.getId());

    }

    @Test
    public void find() throws Exception {

        // data
        UUID gameTypeId = UUID.randomUUID();
        repository.save(TestUtils.createGameConfigEntity(gameTypeId, 10f, 11f, 1000f));
        repository.save(TestUtils.createGameConfigEntity(gameTypeId, 10f, 11f, 1000f));
        repository.save(TestUtils.createGameConfigEntity(UUID.randomUUID(), 10f, 11f, 1000f));

        // call
        GameConfigFilterDto filter = new GameConfigFilterDto();
        filter.setGameTypeId(gameTypeId);
        List<GameConfigResponseDto> gameConfigs = service.find(filter);

        // check
        Assert.assertNotNull(gameConfigs);
        Assert.assertEquals(2, gameConfigs.size());

    }

    @Test
    public void get() throws Exception {

        // data
        GameConfigEntity gameConfig = TestUtils.createGameConfigEntity(UUID.randomUUID(), 10f, 11f, 1000f);
        gameConfig = repository.save(gameConfig);

        // call
        GameConfigResponseDto gameConfigResponseDto = service.get(gameConfig.getId());

        // check
        Assert.assertNotNull(gameConfigResponseDto);
        Assert.assertEquals(gameConfig.getId(), gameConfigResponseDto.getId());

    }

}
