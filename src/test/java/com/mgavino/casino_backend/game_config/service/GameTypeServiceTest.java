package com.mgavino.casino_backend.game_config.service;

import com.mgavino.casino_backend.game_config.repository.GameTypeRepository;
import com.mgavino.casino_backend.game_config.repository.model.GameTypeEntity;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameTypeServiceTest {

    @Autowired
    private GameTypeService service;

    @Autowired
    private GameTypeRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(service);
    }

    @Test
    public void insert() throws Exception {

        GameTypeDto gameTypeDto = TestUtils.createGameTypeDto("Game name", 0.1f);

        // call
        GameTypeResponseDto gameTypeResponseDto = service.insert(gameTypeDto);

        // check
        Assert.assertNotNull(gameTypeResponseDto);
        Assert.assertNotNull(gameTypeResponseDto.getId());

    }

    @Test
    public void getAll() throws Exception {

        // data
        repository.save(TestUtils.createGameTypeEntity("Game Name", 0.1f));
        repository.save(TestUtils.createGameTypeEntity("Game Name 2", 0.1f));
        repository.save(TestUtils.createGameTypeEntity("Game Name 3", 0.1f));

        // call
        List<GameTypeResponseDto> gameTypes = service.getAll();

        // check
        Assert.assertNotNull(gameTypes);
        Assert.assertEquals(3, gameTypes.size());

    }

    @Test
    public void get() throws Exception {

        // data
        GameTypeEntity gameType = repository.save(TestUtils.createGameTypeEntity("Game Name", 0.1f));

        // call
        GameTypeResponseDto gameTypeResponseDto = service.get(gameType.getId());

        // check
        Assert.assertNotNull(gameTypeResponseDto);
        Assert.assertEquals(gameType.getId(), gameTypeResponseDto.getId());

    }

}
