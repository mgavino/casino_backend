package com.mgavino.casino_backend.game_config.service;

import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.game_config.repository.GameConfigRepository;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigFilterDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameConfigServiceFailMockTest {

    @Autowired
    private GameConfigService service;

    @MockBean
    private GameConfigRepository repository;

    @Test(expected = NotFoundException.class)
    public void findNotFound() throws Exception {

        // mock
        Mockito.when(repository.findAll(Mockito.any(Example.class))).thenReturn(new ArrayList());

        // call
        service.find(new GameConfigFilterDto());

    }

    @Test(expected = NotFoundException.class)
    public void getNotFound() throws Exception {

        // mock
        UUID id = UUID.randomUUID();
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

        // call
        service.get(id);

    }

}
