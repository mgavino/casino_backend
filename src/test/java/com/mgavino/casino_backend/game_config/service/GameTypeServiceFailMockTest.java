package com.mgavino.casino_backend.game_config.service;

import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.game_config.repository.GameTypeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameTypeServiceFailMockTest {

    @Autowired
    private GameTypeService service;

    @MockBean
    private GameTypeRepository repository;

    @Test(expected = NotFoundException.class)
    public void getAllNotFound() throws Exception {

        // mock
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());

        // call
        service.getAll();


    }

    @Test(expected = NotFoundException.class)
    public void getNotFound() throws Exception {

        // mock
        UUID gameTypeId = UUID.randomUUID();
        Mockito.when(repository.findById(Mockito.eq(gameTypeId))).thenReturn(Optional.empty());

        // call
        service.get(gameTypeId);

    }

}
