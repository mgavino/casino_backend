package com.mgavino.casino_backend.game_config.repository;

import com.mgavino.casino_backend.game_config.repository.model.GameTypeEntity;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameTypeRepositoryTest {

    @Autowired
    private GameTypeRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(repository);
    }

    @Test
    public void auditing() throws Exception {

        // call
        GameTypeEntity gameType = repository.save(TestUtils.createGameTypeEntity("Name", 0.1F));

        // check
        Assert.assertNotNull(gameType);
        Assert.assertNotNull(gameType.getCreationDate());
        Assert.assertNotNull(gameType.getModificationDate());
        Assert.assertEquals(gameType.getCreationDate(), gameType.getModificationDate());

        // call
        Thread.sleep(100);
        gameType.setProbability(0.2F);
        gameType = repository.save(gameType);

        // check
        Assert.assertNotEquals(gameType.getCreationDate(), gameType.getModificationDate());


    }

}
