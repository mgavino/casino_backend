package com.mgavino.casino_backend.game_config.repository;

import com.mgavino.casino_backend.game_config.repository.model.GameConfigEntity;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameConfigRepositoryTest {

    @Autowired
    private GameConfigRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(repository);
    }

    @Test
    public void auditing() throws Exception{

        // call
        GameConfigEntity gameConfig = repository.save(TestUtils.createGameConfigEntity(UUID.randomUUID(), 10F, 10F, 100F));

        // check
        Assert.assertNotNull(gameConfig);
        Assert.assertNotNull(gameConfig.getCreationDate());
        Assert.assertNotNull(gameConfig.getModificationDate());
        Assert.assertEquals(gameConfig.getCreationDate(), gameConfig.getModificationDate());

        // call
        Thread.sleep(100);
        gameConfig.setPrize(99F);
        gameConfig = repository.save(gameConfig);

        // check
        Assert.assertNotEquals(gameConfig.getCreationDate(), gameConfig.getModificationDate());


    }

}
