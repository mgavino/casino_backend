package com.mgavino.casino_backend.game.service;

import com.mgavino.casino_backend.game.repository.GameRepository;
import com.mgavino.casino_backend.game.repository.model.GameEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.dto.GameDto;
import com.mgavino.casino_backend.game.service.dto.GameFilterDto;
import com.mgavino.casino_backend.game.service.dto.GameResponseDto;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceTest {

    @Autowired
    private GameService service;

    @Autowired
    private GameRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(service);
    }

    @Test
    public void insert() throws Exception {

        GameDto gameDto = TestUtils.createGameDto(UUID.randomUUID(), UUID.randomUUID());

        // call
        GameResponseDto gameResponseDto = service.insert(gameDto);

        // check
        Assert.assertNotNull(gameResponseDto);
        Assert.assertNotNull(gameResponseDto.getId());

    }

    @Test
    public void find() throws Exception {

        // data
        UUID gameConfigId1 = UUID.randomUUID();
        UUID gameConfigId2 = UUID.randomUUID();
        repository.save( TestUtils.createGameEntity(gameConfigId1, UUID.randomUUID(), GameStatus.ACTIVE ) );
        repository.save( TestUtils.createGameEntity(gameConfigId2, UUID.randomUUID(), GameStatus.ACTIVE ) );
        repository.save( TestUtils.createGameEntity(UUID.randomUUID(), UUID.randomUUID(), GameStatus.ACTIVE ) );
        repository.save( TestUtils.createGameEntity(gameConfigId2, UUID.randomUUID(), GameStatus.FINISH ) );

        // call
        GameFilterDto filter = new GameFilterDto();
        filter.setStatus(GameStatus.ACTIVE);
        filter.setGameConfigIds(Arrays.asList(gameConfigId1, gameConfigId2));
        List<GameResponseDto> games = service.find(filter);

        // check
        Assert.assertNotNull(games);
        Assert.assertEquals(2, games.size());

    }

    @Test
    public void get() throws Exception {

        // data
        GameEntity gameEntity = TestUtils.createGameEntity(UUID.randomUUID(), UUID.randomUUID(), GameStatus.ACTIVE );
        gameEntity = repository.save(gameEntity);

        // call
        GameResponseDto game = service.get(gameEntity.getId());

        // check
        Assert.assertNotNull(game);
        Assert.assertEquals(gameEntity.getId(), game.getId());


    }

    @Test
    public void updateStatus() throws Exception {

        // data
        GameEntity gameEntity = TestUtils.createGameEntity(UUID.randomUUID(), UUID.randomUUID(), GameStatus.ACTIVE );
        gameEntity = repository.save(gameEntity);

        // call
        GameResponseDto game = service.updateStatus(gameEntity.getId(), GameStatus.FINISH);

        // check
        Assert.assertNotNull(game);
        Assert.assertEquals(GameStatus.FINISH, game.getStatus());

    }

}
