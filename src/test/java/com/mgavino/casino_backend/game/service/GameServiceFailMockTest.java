package com.mgavino.casino_backend.game.service;

import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.game.repository.GameRepository;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.dto.GameFilterDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceFailMockTest {

    @Autowired
    private GameService service;

    @MockBean
    private GameRepository repository;

    @Value("${casino.max_active_users_per_game}")
    private Integer maxActiveUsersPerGame;

    @Test(expected = NotFoundException.class)
    public void findNotFound() throws Exception {

        // mock
        Mockito.when(repository.findAll(Mockito.any(Example.class))).thenReturn(new ArrayList());

        // call
        service.find(new GameFilterDto());

    }

    @Test(expected = NotFoundException.class)
    public void getNotFound() throws Exception  {

        // mock
        UUID id = UUID.randomUUID();
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

        // call
        service.get(id);

    }

    @Test(expected = NotFoundException.class)
    public void updateStatusNotFound() throws Exception  {

        // mock
        UUID id = UUID.randomUUID();
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

        // call
        service.updateStatus(id, GameStatus.ACTIVE);

    }

}
