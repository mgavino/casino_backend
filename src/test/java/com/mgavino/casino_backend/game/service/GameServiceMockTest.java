package com.mgavino.casino_backend.game.service;

import com.mgavino.casino_backend.core.exceptions.OutRangeMoneyException;
import com.mgavino.casino_backend.game.aop.GameServiceWrapper;
import com.mgavino.casino_backend.game.repository.GameRepository;
import com.mgavino.casino_backend.game.repository.model.GameEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.dto.BetDto;
import com.mgavino.casino_backend.game.service.dto.BetResponseDto;
import com.mgavino.casino_backend.game_config.repository.GameConfigRepository;
import com.mgavino.casino_backend.game_config.repository.model.GameConfigEntity;
import com.mgavino.casino_backend.game_config.service.GameTypeService;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;
import com.mgavino.casino_backend.user.repository.UserRepository;
import com.mgavino.casino_backend.user.repository.model.UserEntity;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceMockTest {

    @Autowired
    private GameServiceWrapper gameService;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GameConfigRepository gameConfigRepository;

    @MockBean
    private GameTypeService gameTypeService;

    @Test
    public void betFail() throws Exception {

        // game entity
        GameEntity gameEntity = generateGameentity(false);

        // call
        BetDto bet = new BetDto();
        bet.setGameId(gameEntity.getId());
        bet.setAmount(30f);
        BetResponseDto betResponseDto = gameService.bet(bet);

        // check
        Assert.assertNotNull(betResponseDto);
        Assert.assertEquals(Float.valueOf(0f), betResponseDto.getPrize());
        Assert.assertEquals(Float.valueOf(70f), betResponseDto.getNewBalance());

    }

    @Test
    public void betPrize () throws Exception {

        // game entity
        GameEntity gameEntity = generateGameentity(true);

        // call
        BetDto bet = new BetDto();
        bet.setGameId(gameEntity.getId());
        bet.setAmount(30f);
        BetResponseDto betResponseDto = gameService.bet(bet);

        // check
        Assert.assertNotNull(betResponseDto);
        Assert.assertEquals(Float.valueOf(1000f), betResponseDto.getPrize());
        Assert.assertEquals(Float.valueOf(1070f), betResponseDto.getNewBalance());

    }

    @Test(expected = OutRangeMoneyException.class)
    public void betOutRangeMoney() throws Exception  {

        // game entity
        GameEntity gameEntity = generateGameentity(false);

        // call
        BetDto bet = new BetDto();
        bet.setGameId(gameEntity.getId());
        bet.setAmount(5f);
        gameService.bet(bet);

    }

    @Test
    public void betBalanceExceeded() throws Exception {

        // game entity
        GameEntity gameEntity = generateGameentity(false);

        // call
        BetDto bet = new BetDto();
        bet.setGameId(gameEntity.getId());
        bet.setAmount(100f);
        BetResponseDto betResponseDto = gameService.bet(bet);

        // check
        Assert.assertNotNull(betResponseDto);
        Assert.assertEquals(Float.valueOf(0f), betResponseDto.getNewBalance());

        // game
        Optional<GameEntity> updatedGameEntity = gameRepository.findById(gameEntity.getId());
        Assert.assertTrue(updatedGameEntity.isPresent());
        Assert.assertEquals(GameStatus.FINISH, updatedGameEntity.get().getStatus());

    }

    @Test
    public void betPlayingtimeExceeded() throws Exception  {

        // game entity
        GameEntity gameEntity = generateGameentity(false);

        // update time user
        Optional<UserEntity> userEntityOpt = userRepository.findById(gameEntity.getUserId());
        Assert.assertTrue(userEntityOpt.isPresent());
        UserEntity userEntity = userEntityOpt.get();
        userEntity.setMaxTime(100l);
        userRepository.save(userEntity);

        // wait (200 ms)
        Thread.sleep(200l);

        // call
        BetDto bet = new BetDto();
        bet.setGameId(gameEntity.getId());
        bet.setAmount(10f);
        gameService.bet(bet);

        // check game status
        Optional<GameEntity> updatedGameEntity = gameRepository.findById(gameEntity.getId());
        Assert.assertTrue(updatedGameEntity.isPresent());
        Assert.assertEquals(GameStatus.FINISH, updatedGameEntity.get().getStatus());

    }

    private GameEntity generateGameentity(boolean hasPrize) throws Exception {

        // mock
        UUID gameTypeId = UUID.randomUUID();
        GameTypeResponseDto gameTypeResponseDto = Mockito.mock(GameTypeResponseDto.class);
        Mockito.when(gameTypeResponseDto.getName()).thenReturn("Name");
        Mockito.when(gameTypeResponseDto.getProbability()).thenReturn(0.1f);
        Mockito.when(gameTypeResponseDto.tryProbability()).thenReturn(hasPrize);
        Mockito.when(gameTypeService.get(gameTypeId)).thenReturn(gameTypeResponseDto);

        // data
        GameConfigEntity gameConfigEntity = TestUtils.createGameConfigEntity(gameTypeId, 10f, 100f, 1000f);
        gameConfigEntity = gameConfigRepository.save(gameConfigEntity);
        UserEntity userEntity = TestUtils.createUserEntity(UUID.randomUUID(), UUID.randomUUID(), 100f, 10000L);
        userEntity = userRepository.save(userEntity);
        GameEntity gameEntity = TestUtils.createGameEntity(gameConfigEntity.getId(), userEntity.getId(), GameStatus.ACTIVE);
        gameEntity = gameRepository.save(gameEntity);

        return gameEntity;

    }



}
