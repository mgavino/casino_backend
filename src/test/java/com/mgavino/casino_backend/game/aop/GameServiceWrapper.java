package com.mgavino.casino_backend.game.aop;

import com.mgavino.casino_backend.game.service.GameService;
import com.mgavino.casino_backend.game.service.dto.BetDto;
import com.mgavino.casino_backend.game.service.dto.BetResponseDto;
import com.mgavino.casino_backend.game.service.dto.GameDto;
import com.mgavino.casino_backend.game.service.dto.GameResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Wrapper to AOP validation
 * AOP intercept as a proxy. Need a intermediary class in the context
 */
@Service
public class GameServiceWrapper {

    @Autowired
    private GameService service;

    public BetResponseDto bet(BetDto betDto) throws Exception {
        return service.bet(betDto);
    }

    public GameResponseDto insert(GameDto gameDto) throws Exception {
        return service.insert(gameDto);
    }



}
