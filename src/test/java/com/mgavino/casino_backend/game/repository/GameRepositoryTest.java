package com.mgavino.casino_backend.game.repository;

import com.mgavino.casino_backend.game.repository.model.GameEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameRepositoryTest {

    @Autowired
    private GameRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(repository);
    }

    @Test
    public void auditing() throws Exception {

        // call
        GameEntity game = repository.save(TestUtils.createGameEntity(UUID.randomUUID(),UUID.randomUUID(), GameStatus.ACTIVE));

        // check
        Assert.assertNotNull(game);
        Assert.assertNotNull(game.getCreationDate());
        Assert.assertNotNull(game.getModificationDate());
        Assert.assertEquals(game.getCreationDate(), game.getModificationDate());

        // call
        Thread.sleep(100);
        game.setStatus(GameStatus.FINISH);
        game = repository.save(game);

        // check
        Assert.assertNotEquals(game.getCreationDate(), game.getModificationDate());

    }

}
