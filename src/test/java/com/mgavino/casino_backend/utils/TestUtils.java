package com.mgavino.casino_backend.utils;

import com.mgavino.casino_backend.game.repository.model.GameEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.dto.GameDto;
import com.mgavino.casino_backend.game_config.repository.model.GameConfigEntity;
import com.mgavino.casino_backend.game_config.repository.model.GameTypeEntity;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeDto;
import com.mgavino.casino_backend.transaction.repository.model.TransactionEntity;
import com.mgavino.casino_backend.transaction.service.dto.TransactionDto;
import com.mgavino.casino_backend.user.repository.model.UserEntity;
import com.mgavino.casino_backend.user.service.dto.UserProviderDto;

import java.util.UUID;

public class TestUtils {

    public static GameEntity createGameEntity(UUID gameConfigId, UUID userId, GameStatus status) {

        GameEntity game = new GameEntity();
        game.setGameConfigId(gameConfigId);
        game.setUserId(userId);
        game.setStatus(status);
        return game;

    }

    public static GameConfigEntity createGameConfigEntity(UUID gameTypeId, Float minBet, Float maxBet, Float prize) {

        GameConfigEntity gameConfig = new GameConfigEntity();
        gameConfig.setGameTypeId(gameTypeId);
        gameConfig.setMinBet(minBet);
        gameConfig.setMaxBet(maxBet);
        gameConfig.setPrize(prize);
        return gameConfig;

    }

    public static GameTypeEntity createGameTypeEntity(String name, Float probability) {

        GameTypeEntity gameType = new GameTypeEntity();
        gameType.setName(name);
        gameType.setProbability(probability);
        return gameType;

    }

    public static TransactionEntity createTransactionEntity(UUID gameTypeId, UUID userId, Float balance, Float amount) {

        TransactionEntity transaction = new TransactionEntity();
        transaction.setGameTypeId(gameTypeId);
        transaction.setUserId(userId);
        transaction.setAmount(amount);
        return transaction;

    }

    public static UserEntity createUserEntity(UUID userId, UUID providerId, Float balance, Long maxTime) {

        UserEntity user = new UserEntity();
        user.setId(userId);
        user.setProviderId(providerId);
        user.setBalance(balance);
        user.setMaxTime(maxTime);
        return user;

    }

    public static GameDto createGameDto(UUID gameConfigId, UUID userId) {

        GameDto game = new GameDto();
        game.setGameConfigId(gameConfigId);
        game.setUserId(userId);
        return game;

    }

    public static GameConfigDto createGameConfigDto(UUID gameTypeId, Float minBet, Float maxBet, Float prize) {

        GameConfigDto gameConfig = new GameConfigDto();
        gameConfig.setGameTypeId(gameTypeId);
        gameConfig.setMinBet(minBet);
        gameConfig.setMaxBet(maxBet);
        gameConfig.setPrize(prize);
        return gameConfig;

    }

    public static GameTypeDto createGameTypeDto(String name, Float probability) {

        GameTypeDto gameType = new GameTypeDto();
        gameType.setName(name);
        gameType.setProbability(probability);
        return gameType;

    }

    public static TransactionDto createTransactionDto(UUID gameTypeId, UUID userId, Float amount) {

        TransactionDto transaction = new TransactionDto();
        transaction.setGameTypeId(gameTypeId);
        transaction.setUserId(userId);
        transaction.setAmount(amount);
        return transaction;

    }

    public static UserProviderDto createUserProviderDto(UUID userId, UUID providerId, Float balance, Long maxTime) {

        UserProviderDto userProvider = new UserProviderDto();
        userProvider.setId(userId);
        userProvider.setProviderId(providerId);
        userProvider.setBalance(balance);
        userProvider.setMaxTime(maxTime);
        return userProvider;

    }

}
