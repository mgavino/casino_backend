package com.mgavino.casino_backend.user.aop;

import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Wrapper to AOP validation
 * AOP intercept as a proxy. Need a intermediary class in the context
 */
@Service
public class UserServiceWrapper {

    @Autowired
    private UserService userService;

    public UserResponseDto updateBalance(UUID id, Float amount) throws Exception {
        return userService.updateBalance(id, amount);
    }

}
