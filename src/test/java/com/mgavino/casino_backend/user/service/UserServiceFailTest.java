package com.mgavino.casino_backend.user.service;

import com.mgavino.casino_backend.core.exceptions.NotEnoughMoneyException;
import com.mgavino.casino_backend.user.aop.UserServiceWrapper;
import com.mgavino.casino_backend.user.repository.UserRepository;
import com.mgavino.casino_backend.user.repository.model.UserEntity;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceFailTest {

    @Autowired
    private UserServiceWrapper service;

    @Autowired
    private UserRepository repository;

    @Test(expected = NotEnoughMoneyException.class)
    public void updateBalanceNotEnoughMoney() throws Exception {

        UserEntity user = TestUtils.createUserEntity(UUID.randomUUID(), UUID.randomUUID(), 10f, 1000L);

        // data
        user = repository.save( user );

        // call
        service.updateBalance(user.getId(), -12f);

    }

}
