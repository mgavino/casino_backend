package com.mgavino.casino_backend.user.service;

import com.mgavino.casino_backend.core.exceptions.AlreadyExistsException;
import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.user.repository.UserRepository;
import com.mgavino.casino_backend.user.repository.model.UserEntity;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceFailMockTest {

    @Autowired
    private UserService service;

    @MockBean
    private UserRepository repository;

    @Test(expected = AlreadyExistsException.class)
    public void insertAlreadyExists() throws Exception {

        UUID userId = UUID.randomUUID();
        UserEntity entity = TestUtils.createUserEntity(userId, UUID.randomUUID(), 10F, 1000L );

        // mock
        Mockito.when(repository.findById(Mockito.eq(userId))).thenReturn(Optional.of(entity));

        // call
        service.insert( TestUtils.createUserProviderDto(userId, UUID.randomUUID(), 10F, 1000L));

    }

    @Test(expected = NotFoundException.class)
    public void getNotFound() throws Exception {

        UUID userId = UUID.randomUUID();

        // mock
        Mockito.when(repository.findById(Mockito.eq(userId))).thenReturn(Optional.empty());

        // call
        service.get(userId);

    }

    @Test(expected = NotFoundException.class)
    public void updateBalanceNotFound() throws Exception {

        UUID userId = UUID.randomUUID();

        // mock
        Mockito.when(repository.findById(Mockito.eq(userId))).thenReturn(Optional.empty());

        // call
        service.updateBalance(userId, 100f);

    }

}
