package com.mgavino.casino_backend.user.service;

import com.mgavino.casino_backend.user.repository.UserRepository;
import com.mgavino.casino_backend.user.repository.model.UserEntity;
import com.mgavino.casino_backend.user.service.dto.UserProviderDto;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService service;

    @Autowired
    private UserRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(service);
    }

    @Test
    public void insert() throws Exception {

        UserProviderDto userProviderDto = TestUtils.createUserProviderDto(UUID.randomUUID(), UUID.randomUUID(), 10F, 1000l);

        // call
        UserResponseDto userResponseDto = service.insert(userProviderDto);

        // check
        Assert.assertNotNull(userResponseDto);
        Assert.assertEquals(userProviderDto.getId(), userResponseDto.getId());

    }

    @Test
    public void get() throws Exception {

        // data
        UserEntity user = TestUtils.createUserEntity(UUID.randomUUID(), UUID.randomUUID(), 10F, 1000l);
        user = repository.save(user);

        // call
        UserResponseDto userResponseDto = service.get(user.getId());

        // check
        Assert.assertNotNull(userResponseDto);
        Assert.assertEquals(user.getId(), userResponseDto.getId());

    }

    @Test
    public void updateBalance() throws Exception {

        // data
        UserEntity user = TestUtils.createUserEntity(UUID.randomUUID(), UUID.randomUUID(), 10F, 1000l);
        user = repository.save(user);

        // call
        UserResponseDto userResponseDto = service.updateBalance(user.getId(), -8f);

        // check
        Assert.assertNotNull(userResponseDto);
        Assert.assertEquals(Float.valueOf(2f), userResponseDto.getBalance());

    }



}
