package com.mgavino.casino_backend.transaction.repository;

import com.mgavino.casino_backend.transaction.repository.model.TransactionEntity;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionRepositoryTest {

    @Autowired
    private TransactionRepository repository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(repository);
    }

    @Test
    public void auditing() {

        // call
        TransactionEntity transaction = repository.save(TestUtils.createTransactionEntity(UUID.randomUUID(), UUID.randomUUID(), 10F, 10F));

        // check
        Assert.assertNotNull(transaction);
        Assert.assertNotNull(transaction.getDate());

    }

}
