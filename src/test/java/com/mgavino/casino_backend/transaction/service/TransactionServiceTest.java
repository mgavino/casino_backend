package com.mgavino.casino_backend.transaction.service;

import com.mgavino.casino_backend.game_config.repository.GameTypeRepository;
import com.mgavino.casino_backend.game_config.repository.model.GameTypeEntity;
import com.mgavino.casino_backend.transaction.service.dto.TransactionDto;
import com.mgavino.casino_backend.transaction.service.dto.TransactionResponseDto;
import com.mgavino.casino_backend.user.repository.UserRepository;
import com.mgavino.casino_backend.user.repository.model.UserEntity;
import com.mgavino.casino_backend.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceTest {

    @Autowired
    private TransactionService service;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GameTypeRepository gameTypeRepository;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(service);
    }

    @Test
    public void insert() throws Exception {

        // data
        UserEntity user = TestUtils.createUserEntity(UUID.randomUUID(), UUID.randomUUID(), 10F, 1000l);
        user = userRepository.save(user);
        GameTypeEntity gameType = TestUtils.createGameTypeEntity("Game Name", 0.1f);
        gameType = gameTypeRepository.save(gameType);

        // call
        TransactionDto transaction = TestUtils.createTransactionDto(gameType.getId(), user.getId(), -8f);
        TransactionResponseDto transactionResponseDto = service.insert(transaction);

        // check
        Assert.assertNotNull(transactionResponseDto);
        Assert.assertEquals(Float.valueOf(-8F), transactionResponseDto.getAmount());

    }

}
