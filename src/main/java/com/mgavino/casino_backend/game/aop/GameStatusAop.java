package com.mgavino.casino_backend.game.aop;

import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.GameService;
import com.mgavino.casino_backend.game.service.dto.BetDto;
import com.mgavino.casino_backend.game.service.dto.BetResponseDto;
import com.mgavino.casino_backend.game.service.dto.GameResponseDto;
import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class GameStatusAop {

    private Logger logger = LoggerFactory.getLogger(GameStatusAop.class);

    @Autowired
    private GameService gameService;

    @Autowired
    private UserService userService;

    /**
     * Check if playing time exceed the max allow
     * @param bet
     * @throws Throwable
     */
    @After("execution(* com.mgavino.casino_backend.game.service.impl.GameServiceImpl.bet(..)) && args(bet)")
    public void checkPlayingTime(BetDto bet) throws Throwable {

        GameResponseDto game = gameService.get(bet.getGameId());
        UserResponseDto user = userService.get(game.getUserId());

        if (game.getPlayingTime() >= user.getMaxTime()) {

            // update game status
            gameService.updateStatus(game.getId(), GameStatus.FINISH);

            // logging
            logger.trace("User playing time exceeded. Game finished.");

        }

    }

    /**
     * Check if user balance is 0
     * @param bet
     * @param betResponse
     * @throws Throwable
     */
    @AfterReturning(value = "execution(* com.mgavino.casino_backend.game.service.impl.GameServiceImpl.bet(..)) && args(bet)", returning = "betResponse")
    public void checkUserBalance(BetDto bet, BetResponseDto betResponse) throws Throwable {

        if (betResponse.getNewBalance() <= 0) {

            // update game status
            gameService.updateStatus(bet.getGameId(), GameStatus.FINISH);

            // logging
            logger.trace("User balance exceeded. Game finished.");

        }

    }

}
