package com.mgavino.casino_backend.game.aop;

import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.GameService;
import com.mgavino.casino_backend.game.service.dto.BetDto;
import com.mgavino.casino_backend.game.service.dto.BetResponseDto;
import com.mgavino.casino_backend.game.service.dto.GameResponseDto;
import com.mgavino.casino_backend.game_config.service.GameConfigService;
import com.mgavino.casino_backend.game_config.service.GameTypeService;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigResponseDto;
import com.mgavino.casino_backend.transaction.service.TransactionService;
import com.mgavino.casino_backend.transaction.service.dto.TransactionDto;
import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class GameTransactionAop {

    private Logger logger = LoggerFactory.getLogger(GameTransactionAop.class);

    @Autowired
    private GameService gameService;

    @Autowired
    private GameConfigService gameConfigService;

    @Autowired
    private TransactionService transactionService;

    /**
     * Insert money transaction
     * @param bet
     * @throws Throwable
     */
    @AfterReturning(value = "execution(* com.mgavino.casino_backend.game.service.impl.GameServiceImpl.bet(..)) && args(bet)", returning = "betResponse")
    public void insertTransaction(BetDto bet, BetResponseDto betResponse) throws Throwable {

        GameResponseDto game = gameService.get(bet.getGameId());
        GameConfigResponseDto gameConfig = gameConfigService.get(game.getGameConfigId());

        // insert bet transaction
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setUserId(game.getUserId());
        transactionDto.setGameTypeId(gameConfig.getGameTypeId());
        transactionDto.setAmount(bet.getAmount() * -1);
        transactionService.insert(transactionDto);

        // insert prize transaction
        if (betResponse.getPrize() > 0) {
            transactionDto.setAmount(betResponse.getPrize());
            transactionService.insert(transactionDto);
        }

    }

}
