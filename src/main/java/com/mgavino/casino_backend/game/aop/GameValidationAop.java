package com.mgavino.casino_backend.game.aop;

import com.mgavino.casino_backend.core.exceptions.MaxActiveUsersException;
import com.mgavino.casino_backend.core.exceptions.OutRangeMoneyException;
import com.mgavino.casino_backend.game.repository.model.GameEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.GameService;
import com.mgavino.casino_backend.game.service.dto.BetDto;
import com.mgavino.casino_backend.game.service.dto.GameFilterDto;
import com.mgavino.casino_backend.game.service.dto.GameResponseDto;
import com.mgavino.casino_backend.game_config.service.GameConfigService;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigFilterDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigResponseDto;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Aspect
@Component
public class GameValidationAop {

    private Logger logger = LoggerFactory.getLogger(GameValidationAop.class);

    @Value("${casino.max_active_users_per_game}")
    private Integer maxActiveUsersPerGame;

    @Autowired
    private GameService gameService;

    @Autowired
    private GameConfigService gameConfigService;

    @Autowired
    private ModelMapper mapper;

    /**
     * Validation: Max active users per game
     * @param game
     * @throws Throwable
     */
    @Before(value = "execution(* com.mgavino.casino_backend.game.service.impl.GameServiceImpl.insert(..)) && args(game)")
    public void checkMaxActiveUsersPerGame(GameEntity game) throws Throwable {

        // get game config
        GameConfigResponseDto gameConfig = gameConfigService.get(game.getGameConfigId());

        // get configs for this type
        GameConfigFilterDto gameConfigFilter = new GameConfigFilterDto();
        gameConfigFilter.setGameTypeId(gameConfig.getGameTypeId());
        List<GameConfigResponseDto> gameConfigs = gameConfigService.find(gameConfigFilter);

        // generate game filter
        GameFilterDto filter = mapper.map(game, GameFilterDto.class);
        filter.setStatus(GameStatus.ACTIVE);
        Set<UUID> gameConfigIds = gameConfigs.stream()
                .map(gameConfigItem -> gameConfigItem.getId())
                .collect(Collectors.toSet());
        filter.setGameConfigIds(gameConfigIds);

        // search active games
        List<GameResponseDto> activeGames = gameService.find(filter);

        // distinct users
        Set<UUID> activeUsers = activeGames.stream()
                .map(activeGame -> activeGame.getUserId())
                .collect(Collectors.toSet());

        if (activeUsers.size() >= maxActiveUsersPerGame) {

            // logging
            logger.trace("Max active users exceeded.");

            // throws exception max active
            throw new MaxActiveUsersException(activeUsers.size(), maxActiveUsersPerGame);
        }

    }

    /**
     * Validation: Bet amount is out range money
     * @param bet
     * @throws Throwable
     */
    @Before("execution(* com.mgavino.casino_backend.game.service.impl.GameServiceImpl.bet(..)) && args(bet)")
    public void checkRangeMoney(BetDto bet) throws Throwable {

        GameResponseDto game = gameService.get(bet.getGameId());
        GameConfigResponseDto gameConfig = gameConfigService.get(game.getGameConfigId());

        if (bet.getAmount() > gameConfig.getMaxBet() ||
                bet.getAmount() < gameConfig.getMinBet()) {

            // logging
            logger.trace("Bet amount out money range.");

            throw new OutRangeMoneyException(bet.getAmount(), gameConfig.getMinBet(), gameConfig.getMaxBet());
        }

    }

}
