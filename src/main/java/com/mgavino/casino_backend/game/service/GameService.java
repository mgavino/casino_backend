package com.mgavino.casino_backend.game.service;

import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.dto.*;

import java.util.List;
import java.util.UUID;

public interface GameService {

    /**
     * Insert new Game
     * @param gameDto
     * @return
     * @throws Exception
     */
    public GameResponseDto insert(GameDto gameDto) throws Exception ;

    /**
     * Find Games by filter
     * @param filterDto
     * @return
     * @throws Exception
     */
    public List<GameResponseDto> find(GameFilterDto filterDto) throws Exception ;

    /**
     * Get Game by ID
     * @param id
     * @return
     * @throws Exception
     */
    public GameResponseDto get(UUID id) throws Exception;

    /**
     * Update Game status
     * @param id
     * @param status
     * @return
     * @throws Exception
     */
    public GameResponseDto updateStatus(UUID id, GameStatus status) throws Exception;

    /**
     * Bet for a specific Game
     * @param betDto
     * @return
     * @throws Exception
     */
    public BetResponseDto bet(BetDto betDto) throws Exception;

}
