package com.mgavino.casino_backend.game.service.dto;

import com.mgavino.casino_backend.game.repository.types.GameStatus;

import java.util.Collection;
import java.util.UUID;

/**
 * DTO used to filter a search of Game
 */
public class GameFilterDto {

    private GameStatus status;
    private Collection<UUID> gameConfigIds;

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public Collection<UUID> getGameConfigIds() {
        return gameConfigIds;
    }

    public void setGameConfigIds(Collection<UUID> gameConfigIds) {
        this.gameConfigIds = gameConfigIds;
    }
}
