package com.mgavino.casino_backend.game.service.dto;

import java.util.UUID;

/**
 * DTO used to create a new Game
 */
public class GameDto {

    private UUID userId;
    private UUID gameConfigId;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getGameConfigId() {
        return gameConfigId;
    }

    public void setGameConfigId(UUID gameConfigId) {
        this.gameConfigId = gameConfigId;
    }
}
