package com.mgavino.casino_backend.game.service.dto;

import com.mgavino.casino_backend.game.repository.types.GameStatus;

import java.util.Date;
import java.util.UUID;

/**
 * DTO used as a Game Response
 */
public class GameResponseDto {

    private UUID id;
    private UUID userId;
    private UUID gameConfigId;
    private GameStatus status;
    private Date creationDate;

    private Long playingTime;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getGameConfigId() {
        return gameConfigId;
    }

    public void setGameConfigId(UUID gameConfigId) {
        this.gameConfigId = gameConfigId;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(Long playingTime) {
        this.playingTime = playingTime;
    }
}
