package com.mgavino.casino_backend.game.service.dto;

import java.util.UUID;

/**
 * DTO used to do a bet
 */
public class BetDto {

    private UUID gameId;
    private Float amount;

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public UUID getGameId() {
        return gameId;
    }

    public void setGameId(UUID gameId) {
        this.gameId = gameId;
    }
}
