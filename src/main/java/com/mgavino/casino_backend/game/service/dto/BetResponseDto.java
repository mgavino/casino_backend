package com.mgavino.casino_backend.game.service.dto;

/**
 * DTO Response for a bet
 */
public class BetResponseDto {

    private Float prize;
    private Float newBalance;

    public Float getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(Float newBalance) {
        this.newBalance = newBalance;
    }

    public Float getPrize() {
        return prize;
    }

    public void setPrize(Float prize) {
        this.prize = prize;
    }
}
