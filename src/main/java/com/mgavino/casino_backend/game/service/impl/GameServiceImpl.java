package com.mgavino.casino_backend.game.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.game.repository.GameRepository;
import com.mgavino.casino_backend.game.repository.model.GameEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.GameService;
import com.mgavino.casino_backend.game.service.dto.*;
import com.mgavino.casino_backend.game_config.service.GameConfigService;
import com.mgavino.casino_backend.game_config.service.GameTypeService;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigResponseDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;
import com.mgavino.casino_backend.game_config.service.impl.GameTypeServiceImpl;
import com.mgavino.casino_backend.transaction.service.TransactionService;
import com.mgavino.casino_backend.transaction.service.dto.TransactionDto;
import com.mgavino.casino_backend.transaction.service.dto.TransactionResponseDto;
import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {

    private Logger logger = LoggerFactory.getLogger(GameTypeServiceImpl.class);

    @Autowired
    private GameRepository repository;

    @Autowired
    private GameConfigService gameConfigService;

    @Autowired
    private GameTypeService gameTypeService;

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Insert new Game
     * @param gameDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public GameResponseDto insert(GameDto gameDto) throws Exception {

        // save
        GameEntity game = mapper.map(gameDto, GameEntity.class);
        game.setStatus(GameStatus.ACTIVE);
        GameEntity savedGame = repository.save(game);

        // log
        logger.trace("insert: " + objectMapper.writeValueAsString(savedGame) );

        // mapping by get method
        return get(savedGame.getId());
    }

    /**
     * Count Games by filter
     * @param filter
     * @return
     * @throws Exception
     */
    public List<GameResponseDto> find(GameFilterDto filter) throws Exception {

        // find
        List<GameEntity> games = repository.findByStatusAndGameConfigIdIn(filter.getStatus(), filter.getGameConfigIds());

        if (!games.isEmpty()) {

            // mapping
            List<GameResponseDto> resultDtos = games.stream()
                    .map( game -> mapper.map(game, GameResponseDto.class) )
                    .collect(Collectors.toList());

            return resultDtos;

        } else {
            throw new NotFoundException("Game");
        }

    }

    /**
     * Get Game by ID
     * @param id
     * @return
     * @throws Exception
     */
    public GameResponseDto get(UUID id) throws Exception {

        // find
        Optional<GameEntity> gameOpt = repository.findById(id);

        if (gameOpt.isPresent()) {

            // mapping
            GameEntity game = gameOpt.get();
            GameResponseDto gameResponseDto = mapper.map(game, GameResponseDto.class);

            // set playing time
            Long now = System.currentTimeMillis();
            Long playingTime = ( now - game.getCreationDate().getTime() );
            gameResponseDto.setPlayingTime(playingTime);

            return gameResponseDto;

        } else {
            throw new NotFoundException("Game");
        }
    }

    /**
     * Update Game status
     * @param id
     * @param status
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public GameResponseDto updateStatus(UUID id, GameStatus status) throws Exception {

        // find
        Optional<GameEntity> gameOpt = repository.findById(id);

        if (gameOpt.isPresent()) {

            // update
            GameEntity game = gameOpt.get();
            game.setStatus(status);
            GameEntity updatedEntity = repository.save(game);

            // log
            logger.trace("updateStatus: " + objectMapper.writeValueAsString(updatedEntity) );

            // mapping by get method
            return get(updatedEntity.getId());

        } else {
            throw new NotFoundException("Game");
        }

    }

    /**
     * Bet for a specific Game
     * @param betDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public BetResponseDto bet(BetDto betDto) throws Exception {

        // get game, config and type
        GameResponseDto game = get(betDto.getGameId());
        GameConfigResponseDto gameConfig = gameConfigService.get(game.getGameConfigId());
        GameTypeResponseDto gameType = gameTypeService.get(gameConfig.getGameTypeId());

        // update user balance
        Float amount = betDto.getAmount() * -1;
        UserResponseDto userResponseDto = userService.updateBalance(game.getUserId(), betDto.getAmount() * -1);

        // bet
        boolean hasPrize = gameType.tryProbability();
        if (hasPrize) {
            // update user balance
            amount = gameConfig.getPrize();
            userResponseDto = userService.updateBalance(game.getUserId(), amount);
        }

        // build response
        BetResponseDto betResponse = new BetResponseDto();
        betResponse.setNewBalance(userResponseDto.getBalance());
        betResponse.setPrize(0F);
        if (amount > 0) {
            // prize only when amount is greater than 0
            betResponse.setPrize(amount);
        }

        return betResponse;

    }
}
