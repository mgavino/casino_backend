package com.mgavino.casino_backend.game.repository.types;

/**
 * Game Status
 * Inform about the status of a Game
 */
public enum GameStatus {
    ACTIVE, FINISH
}
