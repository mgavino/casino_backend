package com.mgavino.casino_backend.game.repository;

import com.mgavino.casino_backend.game.repository.model.GameEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface GameRepository extends JpaRepository<GameEntity, UUID> {

    public List<GameEntity> findByStatusAndGameConfigIdIn(GameStatus status, Collection<UUID> gameConfigIds);

}
