package com.mgavino.casino_backend.game.repository.model;

import com.mgavino.casino_backend.core.repository.model.AuditableEntity;
import com.mgavino.casino_backend.game.repository.types.GameStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.UUID;

/**
 * Game Entity
 * A game for an User and a specific Game Config
 */
@Entity
public class GameEntity extends AuditableEntity {

    @Column(nullable = false)
    private UUID userId;
    @Column(nullable = false)
    private UUID gameConfigId;
    @Column(nullable = false)
    private GameStatus status;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getGameConfigId() {
        return gameConfigId;
    }

    public void setGameConfigId(UUID gameConfigId) {
        this.gameConfigId = gameConfigId;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

}
