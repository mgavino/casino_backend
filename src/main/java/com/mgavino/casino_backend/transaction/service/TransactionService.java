package com.mgavino.casino_backend.transaction.service;

import com.mgavino.casino_backend.transaction.service.dto.TransactionDto;
import com.mgavino.casino_backend.transaction.service.dto.TransactionResponseDto;

public interface TransactionService {

    /**
     * Insert new transaction and refresh user balance
     * @param transactionDto
     * @return
     * @throws Exception
     */
    public TransactionResponseDto insert(TransactionDto transactionDto) throws Exception ;

}
