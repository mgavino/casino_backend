package com.mgavino.casino_backend.transaction.service.dto;

import java.util.UUID;

/**
 * DTO used to create a new transaction
 */
public class TransactionDto {

    private UUID userId;
    private UUID gameTypeId;
    private Float amount;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(UUID gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

}
