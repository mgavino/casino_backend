package com.mgavino.casino_backend.transaction.service.dto;

import java.util.Date;
import java.util.UUID;

/**
 * DTO used as a Transaction response
 */
public class TransactionResponseDto {

    private UUID id;
    private UUID userId;

    private UUID gameTypeId;
    private String gameTypeName;

    private Float amount;

    private Date date;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(UUID gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public String getGameTypeName() {
        return gameTypeName;
    }

    public void setGameTypeName(String gameTypeName) {
        this.gameTypeName = gameTypeName;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
