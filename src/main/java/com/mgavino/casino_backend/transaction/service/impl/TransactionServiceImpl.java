package com.mgavino.casino_backend.transaction.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgavino.casino_backend.game_config.service.GameTypeService;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;
import com.mgavino.casino_backend.transaction.repository.TransactionRepository;
import com.mgavino.casino_backend.transaction.repository.model.TransactionEntity;
import com.mgavino.casino_backend.transaction.service.TransactionService;
import com.mgavino.casino_backend.transaction.service.dto.TransactionDto;
import com.mgavino.casino_backend.transaction.service.dto.TransactionResponseDto;
import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionServiceImpl implements TransactionService {

    private Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Autowired
    private TransactionRepository repository;

    @Autowired
    private GameTypeService gameTypeService;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Insert new transaction and refresh user balance
     * @param transactionDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public TransactionResponseDto insert(TransactionDto transactionDto) throws Exception{

        // create transaction
        TransactionEntity transaction = mapper.map(transactionDto, TransactionEntity.class);
        TransactionEntity savedTransaction = repository.save(transaction);

        // log info
        logger.info("TRANSACTION LOG: " + objectMapper.writeValueAsString(savedTransaction));

        // build response
        TransactionResponseDto transactionResponseDto = mapper.map(savedTransaction, TransactionResponseDto.class);
        GameTypeResponseDto gameType = gameTypeService.get(transactionResponseDto.getGameTypeId());
        transactionResponseDto.setGameTypeName(gameType.getName());

        return transactionResponseDto;
    }

}
