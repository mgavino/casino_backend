package com.mgavino.casino_backend.transaction.repository.model;

import com.mgavino.casino_backend.core.repository.model.IdentifyEntity;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import java.util.Date;
import java.util.UUID;

/**
 * Transaction Entity
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class TransactionEntity extends IdentifyEntity {

    @Column(nullable = false)
    private UUID userId;
    @Column(nullable = false)
    private UUID gameTypeId;
    @Column(nullable = false)
    private Float amount;

    @CreatedDate
    private Date date;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(UUID gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
