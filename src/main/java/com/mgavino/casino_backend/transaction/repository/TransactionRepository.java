package com.mgavino.casino_backend.transaction.repository;

import com.mgavino.casino_backend.transaction.repository.model.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TransactionRepository extends JpaRepository<TransactionEntity, UUID> {
}
