package com.mgavino.casino_backend.core.exceptions;

public class MaxActiveUsersException extends Exception {

    public MaxActiveUsersException(Integer activeUsers, Integer maxUsers) {
        super("Active users (" + activeUsers + ") are limited to " + maxUsers + ".");
    }
}
