package com.mgavino.casino_backend.core.exceptions;

public class AlreadyExistsException extends Exception {

    public AlreadyExistsException(String entity) {
        super("This " + entity + " already exists.");
    }

}
