package com.mgavino.casino_backend.core.exceptions;

public class OutRangeMoneyException extends Exception {

    public OutRangeMoneyException(Float amount, Float min, Float max) {
        super("This amount (" + amount + ") is out of available range (from " + min + " to " + max +").");
    }

}
