package com.mgavino.casino_backend.core.exceptions;

public class NotEnoughMoneyException extends Exception {

    public NotEnoughMoneyException() {
        super("This user don't have enough money.");
    }
}
