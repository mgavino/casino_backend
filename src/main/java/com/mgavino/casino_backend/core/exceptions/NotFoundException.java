package com.mgavino.casino_backend.core.exceptions;

public class NotFoundException extends Exception {

    public NotFoundException(String entity) {
        super(entity + " not found.");
    }

}
