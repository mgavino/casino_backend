package com.mgavino.casino_backend.user.aop;

import com.mgavino.casino_backend.core.exceptions.NotEnoughMoneyException;
import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Aspect
@Component
public class UserValidationAop {

    private Logger logger = LoggerFactory.getLogger(UserValidationAop.class);

    @Autowired
    private UserService userService;

    /**
     * Validation: Check enough balance
     * @param id
     * @param amount
     * @throws Throwable
     */
    @Before(value = "execution(* com.mgavino.casino_backend.user.service.impl.UserServiceImpl.updateBalance(..)) && args(id, amount)")
    public void checkEnoughBalance(UUID id, Float amount) throws Throwable {

        UserResponseDto user = userService.get(id);
        Float newBalance = user.getBalance() + amount;

        // check enough balance
        if (newBalance < 0) {

            // logging
            logger.trace("User don't have enough balance.");

            throw new NotEnoughMoneyException();

        }

    }

}
