package com.mgavino.casino_backend.user.service;

import com.mgavino.casino_backend.user.service.dto.UserProviderDto;

public interface UserProviderService {

    /**
     * Generate new user from a provider
     * @return
     */
    public UserProviderDto get();

}
