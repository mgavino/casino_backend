package com.mgavino.casino_backend.user.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgavino.casino_backend.core.exceptions.AlreadyExistsException;
import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.user.repository.UserRepository;
import com.mgavino.casino_backend.user.repository.model.UserEntity;
import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserProviderDto;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Insert new User
     * @param userProviderDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public UserResponseDto insert(UserProviderDto userProviderDto) throws Exception {

        // map
        UserEntity user = mapper.map(userProviderDto, UserEntity.class);

        // check already exists
        Optional<UserEntity> entityOpt = repository.findById(user.getId());
        if (!entityOpt.isPresent()) {

            // save
            UserEntity savedUser = repository.save(user);

            // log
            logger.trace("insert: " + objectMapper.writeValueAsString(savedUser));

            // mapper
            UserResponseDto userResponseDto = mapper.map(savedUser, UserResponseDto.class);

            return userResponseDto;

        } else {
            throw new AlreadyExistsException("User");
        }

    }

    /**
     * Get User by ID
     * @param id
     * @return
     * @throws Exception
     */
    public UserResponseDto get(UUID id) throws Exception {

        // find
        Optional<UserEntity> userOpt = repository.findById(id);

        if (userOpt.isPresent()) {

            // mapper
            UserEntity user = userOpt.get();
            UserResponseDto userResponseDto = mapper.map(user, UserResponseDto.class);

            return userResponseDto;

        } else {
            throw new NotFoundException("User");
        }

    }

    /**
     * Update User balance
     * @param id
     * @param amount
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public UserResponseDto updateBalance(UUID id, Float amount) throws Exception {

        // find
        Optional<UserEntity> userOpt = repository.findById(id);

        if (userOpt.isPresent()) {

            // new balance
            UserEntity user = userOpt.get();
            Float newBalance = user.getBalance() + amount;
            user.setBalance(newBalance);

            // update
            UserEntity updatedUser = repository.save(user);

            // log
            logger.trace("updateBalance: " + objectMapper.writeValueAsString(updatedUser));

            // mapping
            UserResponseDto userResponseDto = mapper.map(updatedUser, UserResponseDto.class);

            return userResponseDto;

        } else {
            throw new NotFoundException("User");
        }

    }

}
