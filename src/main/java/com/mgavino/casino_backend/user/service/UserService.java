package com.mgavino.casino_backend.user.service;

import com.mgavino.casino_backend.user.service.dto.UserProviderDto;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;

import java.util.UUID;

public interface UserService {

    /**
     * Insert new User
     * @param userProviderDto
     * @return
     * @throws Exception
     */
    public UserResponseDto insert(UserProviderDto userProviderDto) throws Exception ;

    /**
     * Get User by ID
     * @param id
     * @return
     * @throws Exception
     */
    public UserResponseDto get(UUID id) throws Exception ;

    /**
     * Update User balance
     * @param id
     * @param amount
     * @return
     * @throws Exception
     */
    public UserResponseDto updateBalance(UUID id, Float amount) throws Exception;

}
