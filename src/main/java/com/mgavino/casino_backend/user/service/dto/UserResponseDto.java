package com.mgavino.casino_backend.user.service.dto;

import java.util.UUID;

/**
 * DTO used as User response
 */
public class UserResponseDto  {

    private UUID id;
    private UUID providerId;
    private Float balance;
    private Long maxTime;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getProviderId() {
        return providerId;
    }

    public void setProviderId(UUID providerId) {
        this.providerId = providerId;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public Long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(Long maxTime) {
        this.maxTime = maxTime;
    }

}
