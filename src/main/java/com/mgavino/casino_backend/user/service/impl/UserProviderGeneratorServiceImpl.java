package com.mgavino.casino_backend.user.service.impl;

import com.mgavino.casino_backend.user.service.UserProviderService;
import com.mgavino.casino_backend.user.service.dto.UserProviderDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Mock User Provider generator
 */
@Service
public class UserProviderGeneratorServiceImpl implements UserProviderService {

    /**
     * Generate new user from a Mock provider
     * @return
     */
    public UserProviderDto get() {

        // generate user
        UserProviderDto user = new UserProviderDto();
        user.setId(UUID.randomUUID());
        user.setProviderId(UUID.randomUUID());
        user.setBalance(100F); // 100eur
        user.setMaxTime(600000L); // 10 min

        return user;

    }

}
