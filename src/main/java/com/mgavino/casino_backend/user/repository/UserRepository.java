package com.mgavino.casino_backend.user.repository;

import com.mgavino.casino_backend.user.repository.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {
}
