package com.mgavino.casino_backend.user.repository.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * User Entity
 * Used for the balance control and playing maxtime
 */
@Entity
public class UserEntity {

    @Id
    private UUID id;

    @Column(nullable = false)
    private UUID providerId;
    @Column(nullable = false)
    private Float balance;
    @Column(nullable = false)
    private Long maxTime;

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public UUID getProviderId() {
        return providerId;
    }

    public void setProviderId(UUID providerId) {
        this.providerId = providerId;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public Long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(Long maxTime) {
        this.maxTime = maxTime;
    }

}
