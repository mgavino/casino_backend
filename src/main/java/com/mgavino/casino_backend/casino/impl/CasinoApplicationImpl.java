package com.mgavino.casino_backend.casino.impl;

import com.mgavino.casino_backend.casino.CasinoApplication;
import com.mgavino.casino_backend.casino.init.CasinoInitData;
import com.mgavino.casino_backend.casino.service.CasinoService;
import com.mgavino.casino_backend.game.repository.types.GameStatus;
import com.mgavino.casino_backend.game.service.dto.GameResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CasinoApplicationImpl implements CasinoApplication {

	private Logger logger = LoggerFactory.getLogger(CasinoApplicationImpl.class);

	@Autowired
	private CasinoInitData casinoInitData;

	@Autowired
	private CasinoService casino;

	public void run() throws Exception {

		// init data
		casinoInitData.registerGames();

		// generate user
		UUID userId = casino.generateUser();

		// select game type
		UUID gameTypeId = casino.selectGameType();

		// select game config
		UUID gameConfigId = casino.selectGameConfig(gameTypeId);

		// start game
		GameResponseDto game = casino.startGame(userId, gameConfigId);

		// while game is ACTIVE
		while(GameStatus.ACTIVE.equals(game.getStatus())) {

			// let's bet
			game = casino.tryBet( game.getId() );

		}

		// finish
		logger.info("Game finished");

	}

}
