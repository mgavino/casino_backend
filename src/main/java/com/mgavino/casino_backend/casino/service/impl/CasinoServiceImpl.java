package com.mgavino.casino_backend.casino.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgavino.casino_backend.casino.service.CasinoService;
import com.mgavino.casino_backend.game.service.GameService;
import com.mgavino.casino_backend.game.service.dto.BetDto;
import com.mgavino.casino_backend.game.service.dto.BetResponseDto;
import com.mgavino.casino_backend.game.service.dto.GameDto;
import com.mgavino.casino_backend.game.service.dto.GameResponseDto;
import com.mgavino.casino_backend.game_config.service.GameConfigService;
import com.mgavino.casino_backend.game_config.service.GameTypeService;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigFilterDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigResponseDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;
import com.mgavino.casino_backend.user.service.UserProviderService;
import com.mgavino.casino_backend.user.service.UserService;
import com.mgavino.casino_backend.user.service.dto.UserProviderDto;
import com.mgavino.casino_backend.user.service.dto.UserResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;

@Component
public class CasinoServiceImpl implements CasinoService {

    private Logger logger = LoggerFactory.getLogger(CasinoServiceImpl.class);

    @Autowired
    private UserProviderService userProviderService;

    @Autowired
    private UserService userService;

    @Autowired
    private GameService gameService;

    @Autowired
    private GameTypeService gameTypeService;

    @Autowired
    private GameConfigService gameConfigService;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Generate an User and return its ID
     * @return
     * @throws Exception
     */
    public UUID generateUser() throws Exception {

        // Generate user from user provider
        UserProviderDto userProviderDto = userProviderService.get();

        // insert user in our DDBB (to check balance control)
        UserResponseDto user = userService.insert(userProviderDto);

        return user.getId();

    }

    /**
     * Show available Game Types and ask for one of them.
     * Return the selected Game Type ID
     * @return
     * @throws Exception
     */
    public UUID selectGameType() throws Exception {

        Scanner scanner = new Scanner(System.in);

        UUID gameTypeId = null;

        while (gameTypeId == null) {

            // show game types
            List<GameTypeResponseDto> gameTypes = gameTypeService.getAll();
            for (int i = 0; i < gameTypes.size(); i++) {
                System.out.println( (i + 1) + ". " + gameTypes.get(i).getName());
            }

            // read game type
            System.out.print("Select a game type: ");
            int pos = scanner.nextInt() - 1;

            // check exists
            if (gameTypes.size() > pos && pos >= 0) {
                gameTypeId = gameTypes.get(pos).getId();
            }

        }

        return gameTypeId;

    }

    /**
     * Show available Game Configs for the given Game Type
     * and ask for one of them.
     * Return the selected Game Config ID
     * @param gameTypeId
     * @return
     * @throws Exception
     */
    public UUID selectGameConfig(UUID gameTypeId) throws Exception {

        Scanner scanner = new Scanner(System.in);

        UUID gameConfigId = null;

        while (gameConfigId == null) {

            // show configs
            GameConfigFilterDto filter = new GameConfigFilterDto();
            filter.setGameTypeId(gameTypeId);
            List<GameConfigResponseDto> gameConfigs = gameConfigService.find(filter);

            for (int i = 0; i < gameConfigs.size(); i++) {
                System.out.println( (i+1) + ". PRIZE: " + gameConfigs.get(i).getPrize());
            }

            // read game config
            System.out.print("Select a game config: ");
            int pos = scanner.nextInt() - 1;

            // check exists
            if (gameConfigs.size() > pos && pos >= 0) {
                gameConfigId = gameConfigs.get(pos).getId();
            }

        }

        return gameConfigId;

    }

    /**
     * Start game with the given User ID and Game Config ID
     * @param userId
     * @param gameConfigId
     * @return
     * @throws Exception
     */
    public GameResponseDto startGame(UUID userId, UUID gameConfigId) throws Exception {

        // create new game
        GameDto gameDto = new GameDto();
        gameDto.setUserId(userId);
        gameDto.setGameConfigId(gameConfigId);
        return gameService.insert(gameDto);

    }

    /**
     * Ask for a new bet for the given Game ID
     * Return the updated Game after apply bet
     * @param gameId
     * @return
     * @throws Exception
     */
    public GameResponseDto tryBet(UUID gameId) throws Exception {

        // bet
        try {
            bet(gameId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        // get updated game instance
        GameResponseDto game = gameService.get( gameId );

        // logger new playing time and status
        logger.info( "game: " + "{\"playingTime\":\"" +  game.getPlayingTime() + "\", \"status\":\"" + game.getStatus() + "\"} ");

        return game;

    }

    private void bet(UUID gameId) throws Exception {

        Scanner scanner = new Scanner(System.in);

        // read bet value
        System.out.print("Insert your bet: ");
        Float amount = scanner.nextFloat();

        // try bet
        BetDto betDto = new BetDto();
        betDto.setGameId(gameId);
        betDto.setAmount(amount);
        BetResponseDto betResponseDto = gameService.bet(betDto);

        // logger result
        logger.info("bet: " + objectMapper.writeValueAsString(betResponseDto) );

    }

}
