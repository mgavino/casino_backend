package com.mgavino.casino_backend.casino.service;

import com.mgavino.casino_backend.game.service.dto.GameResponseDto;

import java.util.UUID;

/**
 * Casino Service
 * Used to read and write values from System.out
 */
public interface CasinoService {

    /**
     * Generate an User and return its ID
     * @return
     * @throws Exception
     */
    public UUID generateUser() throws Exception;

    /**
     * Show available Game Types and ask for one of them.
     * Return the selected Game Type ID
     * @return
     * @throws Exception
     */
    public UUID selectGameType() throws Exception;

    /**
     * Show available Game Configs for the given Game Type
     * and ask for one of them.
     * Return the selected Game Config ID
     * @param gameTypeId
     * @return
     * @throws Exception
     */
    public UUID selectGameConfig(UUID gameTypeId) throws Exception;

    /**
     * Start game with the given User ID and Game Config ID
     * @param userId
     * @param gameConfigId
     * @return
     * @throws Exception
     */
    public GameResponseDto startGame(UUID userId, UUID gameConfigId) throws Exception;

    /**
     * Ask for a new bet for the given Game ID
     * Return the updated Game after apply bet
     * @param gameId
     * @return
     * @throws Exception
     */
    public GameResponseDto tryBet(UUID gameId) throws Exception ;
}
