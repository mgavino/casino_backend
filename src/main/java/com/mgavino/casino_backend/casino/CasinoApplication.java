package com.mgavino.casino_backend.casino;

public interface CasinoApplication {

    public void run() throws Exception;
}
