package com.mgavino.casino_backend.casino.init;

import com.mgavino.casino_backend.game_config.service.GameConfigService;
import com.mgavino.casino_backend.game_config.service.GameTypeService;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;

/**
 * Init data autogeneration
 */
@Component
public class CasinoInitData {

    @Value("#{'${casino.game_types}'.split(',')}")
    private List<String> gameTypeNames;

    @Value("${casino.configs_per_game}")
    private Integer configsPerGame;

    @Value("${casino.game_type_min_probability}")
    private Float gameTypeMinProbability;
    @Value("${casino.game_type_max_probability}")
    private Float gameTypeMaxProbability;

    @Autowired
    private GameTypeService gameTypeService;

    @Autowired
    private GameConfigService gameConfigService;

    public void registerGames() throws Exception {

        Random random = new Random();

        // Register Game Types
        registerGameTypes();

        // Register Game Configs
        registerGameConfigs();

    }

    /**
     * Register Game Types: VIDEOBINGO, SLOT, BLACKJACK, POCKER, RULETA
     * Probability is a random between 0.1 and 0.3
     * @throws Exception
     */
    private void registerGameTypes() throws Exception {
        Random random = new Random();

        // Register Game Types
        GameTypeDto typeDto;
        for (String gameTypeName : gameTypeNames) {

            typeDto = new GameTypeDto();
            typeDto.setName(gameTypeName);

            Float probability = (gameTypeMinProbability + random.nextFloat() * (gameTypeMaxProbability - gameTypeMinProbability));
            probability = new BigDecimal(probability).setScale(2, RoundingMode.HALF_UP).floatValue();
            typeDto.setProbability(probability);

            gameTypeService.insert(typeDto);

        }

    }

    /**
     *
     * @throws Exception
     */
    private void registerGameConfigs() throws Exception {

        Random random = new Random();

        List<GameTypeResponseDto> gameTypes = gameTypeService.getAll();
        GameConfigDto configDto;
        for (GameTypeResponseDto gameType : gameTypes) {

            for (int i = 0; i < configsPerGame; i++) {

                configDto = new GameConfigDto();
                configDto.setGameTypeId(gameType.getId());

                Float minBet = random.nextFloat() * 10;
                minBet = new BigDecimal(minBet).setScale(2, RoundingMode.HALF_UP).floatValue();
                configDto.setMinBet(minBet);

                Float maxBet = configDto.getMinBet() + (random.nextFloat() * 100);
                maxBet = new BigDecimal(maxBet).setScale(2, RoundingMode.HALF_UP).floatValue();
                configDto.setMaxBet( maxBet );

                Float prize = random.nextFloat() * 1000;
                prize = new BigDecimal(prize).setScale(2, RoundingMode.HALF_UP).floatValue();
                configDto.setPrize(prize);

                gameConfigService.insert(configDto);

            }

        }

    }

}
