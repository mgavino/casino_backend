package com.mgavino.casino_backend.game_config.repository.model;

import com.mgavino.casino_backend.core.repository.model.AuditableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.UUID;

/**
 * Game Config Entity
 * Game type + configuration parameters
 */
@Entity
public class GameConfigEntity extends AuditableEntity {

    @Column(nullable = false)
    private UUID gameTypeId;

    // config
    @Column(nullable = false)
    private Float prize;
    @Column(nullable = false)
    private Float minBet;
    @Column(nullable = false)
    private Float maxBet;

    public UUID getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(UUID gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public Float getPrize() {
        return prize;
    }

    public void setPrize(Float prize) {
        this.prize = prize;
    }

    public Float getMinBet() {
        return minBet;
    }

    public void setMinBet(Float minBet) {
        this.minBet = minBet;
    }

    public Float getMaxBet() {
        return maxBet;
    }

    public void setMaxBet(Float maxBet) {
        this.maxBet = maxBet;
    }

}
