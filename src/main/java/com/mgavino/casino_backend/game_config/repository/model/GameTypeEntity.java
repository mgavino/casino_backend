package com.mgavino.casino_backend.game_config.repository.model;

import com.mgavino.casino_backend.core.repository.model.AuditableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Game Type Entity
 */
@Entity
public class GameTypeEntity extends AuditableEntity {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Float probability;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getProbability() {
        return probability;
    }

    public void setProbability(Float probability) {
        this.probability = probability;
    }
}
