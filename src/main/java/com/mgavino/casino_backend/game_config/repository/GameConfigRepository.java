package com.mgavino.casino_backend.game_config.repository;

import com.mgavino.casino_backend.game_config.repository.model.GameConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GameConfigRepository extends JpaRepository<GameConfigEntity, UUID> {
}
