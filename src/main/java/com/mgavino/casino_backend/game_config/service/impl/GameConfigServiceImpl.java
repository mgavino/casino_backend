package com.mgavino.casino_backend.game_config.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.game_config.repository.GameConfigRepository;
import com.mgavino.casino_backend.game_config.repository.model.GameConfigEntity;
import com.mgavino.casino_backend.game_config.service.GameConfigService;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigFilterDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigResponseDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GameConfigServiceImpl implements GameConfigService {

    private Logger logger = LoggerFactory.getLogger(GameTypeServiceImpl.class);

    @Autowired
    private GameConfigRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Insert new Game Config
     * @param gameConfigDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public GameConfigResponseDto insert(GameConfigDto gameConfigDto) throws Exception {

        // save
        GameConfigEntity gameConfig = mapper.map(gameConfigDto, GameConfigEntity.class);
        GameConfigEntity savedGameConfig = repository.save(gameConfig);

        // log
        logger.trace("GameConfigService.insert: " + objectMapper.writeValueAsString(savedGameConfig) );

        // mapping
        GameConfigResponseDto gameConfigResponseDto = mapper.map(savedGameConfig, GameConfigResponseDto.class);

        return gameConfigResponseDto;

    }

    /**
     * Find Game Configs by filter
     * @param filter
     * @return
     * @throws Exception
     */
    public List<GameConfigResponseDto> find(GameConfigFilterDto filter) throws Exception {

        // find
        GameConfigEntity probe = mapper.map(filter, GameConfigEntity.class);
        Example<GameConfigEntity> example = Example.of(probe);
        List<GameConfigEntity> gameConfigs = repository.findAll(example);

        if (!gameConfigs.isEmpty()) {

            // mapping
            List<GameConfigResponseDto> resultDtos = gameConfigs.stream()
                    .map( gameConfig -> mapper.map(gameConfig, GameConfigResponseDto.class) )
                    .collect(Collectors.toList());

            return resultDtos;

        } else {
            throw new NotFoundException("Game Config");
        }

    }

    /**
     * Get Game Config by ID
     * @param id
     * @return
     * @throws Exception
     */
    public GameConfigResponseDto get(UUID id) throws Exception {

        // find
        Optional<GameConfigEntity> gameConfigOpt = repository.findById(id);

        if (gameConfigOpt.isPresent()) {

            GameConfigEntity gameConfig = gameConfigOpt.get();

            // mapping
            GameConfigResponseDto gameConfigResponseDto = mapper.map(gameConfig, GameConfigResponseDto.class);

            return gameConfigResponseDto;

        } else {
            throw new NotFoundException("Game Config");
        }
    }

}
