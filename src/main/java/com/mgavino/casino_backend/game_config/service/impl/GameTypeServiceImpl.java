package com.mgavino.casino_backend.game_config.service.impl;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgavino.casino_backend.core.exceptions.NotFoundException;
import com.mgavino.casino_backend.game_config.repository.GameTypeRepository;
import com.mgavino.casino_backend.game_config.repository.model.GameTypeEntity;
import com.mgavino.casino_backend.game_config.service.GameTypeService;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GameTypeServiceImpl implements GameTypeService {

    private Logger logger = LoggerFactory.getLogger(GameTypeServiceImpl.class);

    @Autowired
    private GameTypeRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Insert new Game Type
     * @param gameTypeDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public GameTypeResponseDto insert(GameTypeDto gameTypeDto) throws Exception {

        // save
        GameTypeEntity gameType = mapper.map(gameTypeDto, GameTypeEntity.class);
        GameTypeEntity savedGameType = repository.save(gameType);

        // log
        logger.trace("insert: " + objectMapper.writeValueAsString(savedGameType) );

        // mapping
        GameTypeResponseDto gameTypeResponseDto = mapper.map(savedGameType, GameTypeResponseDto.class);

        return gameTypeResponseDto;
    }

    /**
     * Get all Game Types
     * @return
     * @throws Exception
     */
    public List<GameTypeResponseDto> getAll() throws Exception {

        // find
        List<GameTypeEntity> gameTypes = repository.findAll();

        if (!gameTypes.isEmpty()) {

            // mapping
            List<GameTypeResponseDto> resultDtos = gameTypes.stream()
                    .map( gameType -> mapper.map(gameType, GameTypeResponseDto.class) )
                    .collect(Collectors.toList());

            return resultDtos;

        } else {
            throw new NotFoundException("Game Type");
        }

    }

    /**
     * Get Game Type by ID
     * @param id
     * @return
     * @throws Exception
     */
    public GameTypeResponseDto get(UUID id) throws Exception {

        // find
        Optional<GameTypeEntity> gameTypeOpt = repository.findById(id);

        if (gameTypeOpt.isPresent()) {

            // mapping
            GameTypeEntity gameType = gameTypeOpt.get();
            GameTypeResponseDto gameTypeResponseDto = mapper.map(gameType, GameTypeResponseDto.class);

            return gameTypeResponseDto;

        } else {
            throw new NotFoundException("Game Type");
        }

    }

}
