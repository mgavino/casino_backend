package com.mgavino.casino_backend.game_config.service.dto;

/**
 * DTO used to create a new Game Type
 */
public class GameTypeDto {

    private String name;
    private Float probability;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getProbability() {
        return probability;
    }

    public void setProbability(Float probability) {
        this.probability = probability;
    }

}
