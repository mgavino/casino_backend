package com.mgavino.casino_backend.game_config.service.dto;

import java.util.UUID;

/**
 * DTO used to filter a search of Game Config
 */
public class GameConfigFilterDto {

    private UUID gameTypeId;

    public UUID getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(UUID gameTypeId) {
        this.gameTypeId = gameTypeId;
    }
}