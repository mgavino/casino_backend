package com.mgavino.casino_backend.game_config.service.dto;

import java.util.UUID;

/**
 * DTO used to create a new Game Config
 */
public class GameConfigDto {

    private UUID gameTypeId;

    // config
    private Float prize;
    private Float minBet;
    private Float maxBet;

    public UUID getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(UUID gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public Float getPrize() {
        return prize;
    }

    public void setPrize(Float prize) {
        this.prize = prize;
    }

    public Float getMinBet() {
        return minBet;
    }

    public void setMinBet(Float minBet) {
        this.minBet = minBet;
    }

    public Float getMaxBet() {
        return maxBet;
    }

    public void setMaxBet(Float maxBet) {
        this.maxBet = maxBet;
    }
}
