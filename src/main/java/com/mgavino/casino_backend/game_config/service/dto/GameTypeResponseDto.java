package com.mgavino.casino_backend.game_config.service.dto;

import java.util.Random;
import java.util.UUID;

/**
 * DTO used as a Game Type response
 */
public class GameTypeResponseDto {

    private UUID id;
    private String name;
    private Float probability;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getProbability() {
        return probability;
    }

    public void setProbability(Float probability) {
        this.probability = probability;
    }

    /**
     * Check success using probability
     */
    public boolean tryProbability() {
        Random random = new Random();
        return (random.nextFloat() <= getProbability());
    }
}
