package com.mgavino.casino_backend.game_config.service;

import com.mgavino.casino_backend.game_config.service.dto.GameConfigDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigFilterDto;
import com.mgavino.casino_backend.game_config.service.dto.GameConfigResponseDto;

import java.util.List;
import java.util.UUID;

public interface GameConfigService {

    /**
     * Insert new Game Config
     * @param gameConfigDto
     * @return
     * @throws Exception
     */
    public GameConfigResponseDto insert(GameConfigDto gameConfigDto) throws Exception ;

    /**
     * Find Game Configs by filter
     * @param filter
     * @return
     * @throws Exception
     */
    public List<GameConfigResponseDto> find(GameConfigFilterDto filter) throws Exception ;

    /**
     * Get Game Config by ID
     * @param id
     * @return
     * @throws Exception
     */
    public GameConfigResponseDto get(UUID id) throws Exception ;

}
