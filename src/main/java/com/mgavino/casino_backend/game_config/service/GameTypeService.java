package com.mgavino.casino_backend.game_config.service;

import com.mgavino.casino_backend.game_config.service.dto.GameTypeDto;
import com.mgavino.casino_backend.game_config.service.dto.GameTypeResponseDto;

import java.util.List;
import java.util.UUID;

public interface GameTypeService {

    /**
     * Insert new Game Type
     * @param gameTypeDto
     * @return
     * @throws Exception
     */
    public GameTypeResponseDto insert(GameTypeDto gameTypeDto) throws Exception;

    /**
     * Get all Game Types
     * @return
     * @throws Exception
     */
    public List<GameTypeResponseDto> getAll() throws Exception;

    public GameTypeResponseDto get(UUID id) throws Exception;

}
